/* eslint-disable require-jsdoc */
const HttpError = require('../utils/httpError.js');
const {query, getColumnSet} = require('./db.js');

/**
 * @typedef User
 * @property {integer}
 * @property {string} email.required - user email
 * @property {string} firstName.required - user firstName
 * @property {string} lastName.required - user lastName
 * @property {string} password.required - user password
 */

class User {
  constructor() {
    this.tableName = 'users';
  }

  async create(user) {
    const result= await query(`INSERT INTO ${this.tableName}
     (email, firstName, lastName, password) VALUES (?, ?, ?, ?)`,
    [user.email, user.firstName, user.lastName, user.password]);
    return {...user, id: result.insertId};
  };

  async findById(userId) {
    const result= await query(`SELECT * FROM  users WHERE id = ${userId}`);
    return result[0];
  };

  async find() {
    return await query(`SELECT * FROM ${this.tableName}`);
  };

  async updateById(id, user) {
    const {columns, values}=getColumnSet(user);
    const result= await query(
        `UPDATE ${this.tableName} SET ${columns} WHERE id = ?`,
        [...values, id] );
    if (result.affectedRows==0) throw new HttpError('User not found');
    return result;
  };

  async findOne(params) {
    const {columns, values}=getColumnSet(params);
    const result= await query(
        `SELECT * from ${this.tableName} WHERE ${columns} `, values );
    return result[0];
  }

  async remove(id) {
    const result= await query(`DELETE FROM ${this.tableName} WHERE id = ?`,
        [id]);
    if (result.affectedRows==0) throw new HttpError('User not found');
    return result;
  };

  async removeAll() {
    return await query(`DELETE FROM ${this.tableName}`);
  };
}

module.exports = new User();
