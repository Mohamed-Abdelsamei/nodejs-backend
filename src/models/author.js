/* eslint-disable require-jsdoc */
const HttpError = require('../utils/httpError.js');
const {query, getColumnSet} = require('./db.js');
/**
 * @typedef Author
 * @property {integer} id
 * @property {string} name.required - author name
 * @property {string} title.required - author title
 * @property {string} job.required - author job
 */

class Author {
  constructor() {
    this.tableName = 'authors';
  }

  async create(author) {
    const result= await query(`INSERT INTO ${this.tableName}
     (name, title, job) VALUES (?, ?, ?)`,
    [author.name, author.title, author.job]);
    // console.log(result.insertId);
    return {...author, id: result.insertId};
  };

  async findById(authorId) {
    const result= await query(`SELECT * FROM  authors WHERE id = ${authorId}`);
    return result[0];
  };

  async find() {
    return await query(`SELECT * FROM  ${this.tableName}`);
  };

  async updateById(id, author) {
    const {columns, values}=getColumnSet(author);
    const result= await query(
        `UPDATE ${this.tableName} SET ${columns} WHERE id = ?`,
        [...values, id] );
    if (result.affectedRows==0) throw new HttpError(400, 'Author not found');
    return result;
  };

  async remove(id) {
    const result= await query(
        `DELETE FROM  ${this.tableName} WHERE id = ?`, [id]);

    if (result.affectedRows==0) throw new HttpError(400, 'Author not found');
    return result;
  };

  async removeAll() {
    return await query(`DELETE FROM  ${this.tableName}`);
  };
}

module.exports = new Author();
