/* eslint-disable require-jsdoc */
const mysql = require('mysql2/promise');
const config = require('../config');

const pool = mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.db,
  namedPlaceholders: true,
});

async function query(sql, params) {
  const [rows] = await pool.execute(sql, params);
  return rows;
}

function getColumnSet(object) {
  if (typeof object !== 'object') {
    throw new Error('Invalid input');
  }

  const keys = Object.keys(object);
  const values = Object.values(object);

  columns = keys.map((key) => `${key} = ?`).join(', ');

  return {
    columns,
    values,
  };
}
module.exports = {query, getColumnSet};
