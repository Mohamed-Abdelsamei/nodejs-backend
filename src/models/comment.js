/* eslint-disable require-jsdoc */
const HttpError = require('../utils/httpError.js');
const {query, getColumnSet} = require('./db.js');

/**
 * @typedef Comment
 * @property {integer} userId.required
 * @property {integer} articleId.required
 * @property {string} text.required
 */

class Comment {
  constructor() {
    this.tableName = 'comments';
  }

  async create( comment) {
    const result= await query(`INSERT INTO ${this.tableName}
     (text, userId, articleId) VALUES (?, ?, ?)`,
    [comment.text, comment.userId, comment.articleId]);
    // console.log(result.insertId);
    return {...comment, id: result.insertId};
  };

  async findByArticleId( articleId) {
    const result=
     await query(`SELECT * FROM   comments WHERE articleId = ${ articleId}`);
    return result;
  };

  async findByUserId( userId) {
    const result=
     await query(`SELECT * FROM   comments WHERE userId = ${ userId}`);
    return result;
  };

  async find() {
    return await query(`SELECT * FROM  ${this.tableName}`);
  };

  async updateById(id, comment) {
    const {columns, values}=getColumnSet( comment);
    const result= await query(
        `UPDATE ${this.tableName} SET ${columns} WHERE id = ?`,
        [...values, id] );
    if (result.affectedRows==0) throw new HttpError(400, ' Comment not found');
    return result;
  };

  async remove(id) {
    const result= await query(
        `DELETE FROM  ${this.tableName} WHERE id = ?`, [id]);

    if (result.affectedRows==0) throw new HttpError(400, ' Comment not found');
    return result;
  };

  async removeAll() {
    return await query(`DELETE FROM  ${this.tableName}`);
  };
}

module.exports = new Comment();
