/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
const HttpError = require('../utils/httpError.js');
const {query} = require('./db.js');

/**
 * @typedef Thumb
 * @property {integer} userId.required
 * @property {integer} articleId.required
 */

class ThumbedArticles {
  constructor() {
    this.tableName = 'thumbs';
  }

  async create( thumb) {
    const result= await query(`INSERT INTO ${this.tableName}
     (userId, articleId) VALUES (?, ?)`,
    [thumb.userId, thumb.articleId]);
    return {...thumb, id: result.insertId};
  };

  async findByArticleId( articleId) {
    const result=
     await query(`SELECT * FROM ${this.tableName} WHERE articleId = ${ articleId}`);
    return result;
  };

  async findByUserIdAndArticleId( userId, articleId) {
    const result=
     await query(`SELECT * FROM   ${this.tableName} WHERE userId =
      ${ userId} AND articleId = ${articleId}`);
    return result[0];
  };


  async remove(id) {
    const result= await query(
        `DELETE FROM  ${this.tableName} WHERE id = ?`, [id]);

    if (result.affectedRows==0) throw new HttpError(400, ' Comment not found');
    return result;
  };

  async removeAll() {
    return await query(`DELETE FROM  ${this.tableName}`);
  };
}

module.exports = new ThumbedArticles();
