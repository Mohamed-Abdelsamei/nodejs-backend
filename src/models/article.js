/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
const HttpError = require('../utils/httpError.js');
const {query, getColumnSet} = require('./db.js');

/**
 * @typedef Article
 * @property {integer} id
 * @property {string} title.required - Some description for product
 * @property {string} body.required - Some description for product
 * @property {integer} authorId.required - Some description for product
 */

class Article {
  constructor() {
    this.tableName = 'articles';
  }

  async create(article) {
    const result= await query(`INSERT INTO ${this.tableName}
     (title, body, authorId) VALUES (?, ?, ?)`,
    [article.title, article.body, article.authorId]);
    // console.log(result.insertId);
    return {...article, id: result.insertId};
  };

  async findById(articleId) {
    const result=
     await query(`SELECT t1.*, ( SELECT count(*) FROM thumbs t2 WHERE t2.articleId = t1.id ) As thumbs FROM ${this.tableName} t1 WHERE id = ${articleId}`);
    return result[0];
  };

  async find() {
    return await query(`SELECT t1.*, ( SELECT count(*) FROM thumbs t2 WHERE t2.articleId = t1.id ) As thumbs FROM  ${this.tableName} t1`);
  };

  async search(text) {
    return await query(`SELECT t1.*, ( SELECT count(*) FROM thumbs t2 WHERE t2.articleId = t1.id ) As thumbs FROM ${this.tableName} WHERE title = ?`,
        [text]);
  };

  async updateById(id, article) {
    const {columns, values}=getColumnSet(article);
    const result= await query(
        `UPDATE ${this.tableName} SET ${columns} WHERE id = ?`,
        [...values, id] );
    if (result.affectedRows==0) throw new HttpError(400, 'Article not found');
    return result;
  };

  async remove(id) {
    const result= await query(
        `DELETE FROM  ${this.tableName} WHERE id = ?`, [id]);

    if (result.affectedRows==0) throw new HttpError(400, 'Article not found');
    return result;
  };

  async removeAll() {
    return await query(`DELETE FROM  ${this.tableName}`);
  };
}

module.exports = new Article();
