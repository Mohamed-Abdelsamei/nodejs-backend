const express = require('express');
const validate = require('../middlewares/validate');

const router = new express.Router();
const controller = require('../controllers/user');
const validation = require('../validations/user');

const authenticateJWT = require('../middlewares/authenticateJWT');
/**
 * This function comment is parsed by doctrine
 * @route GET /users get all users
 * @group Users
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */

router.get('/',
    controller.list);
/**
 * This function comment is parsed by doctrine
 * @route POST /users add ne user
 * @group Users
 * @param {string} email.body.required - user email
 * @param {string} firstName.body.required - user firstName
 * @param {string} lastName.body.required - user lastName
 * @param {string} password.body.required - user password
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */

router.post('/',
    validate(validation.create),
    controller.create);

/**
 * This function comment is parsed by doctrine
 * @route GET /users/me view my profile
 * @group Users
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get('/me',
    authenticateJWT,
    controller.me);

/**
 * This function comment is parsed by doctrine
 * @route GET /users/{userId} get user info by id
 * @group Users
 * @param {number} userId.params.required - user id
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */

router.get('/:userId',
    controller.findById);

/**
 * This function comment is parsed by doctrine
 * @route PUT /users/{userId} update user by id
 * @group Users
 * @param {number} userId.params.required - user id
 * @param {string} email.body - user email
 * @param {string} firstName.body - user firstName
 * @param {string} lastName.body - user lastName
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */

router.put('/:userId',
    validate(validation.update),
    controller.update);
/**
 * This function comment is parsed by doctrine
 * @route DELETE /users/{userId} delete user by id
 * @group Users
 * @param {number} userId.params.required - user id
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */

router.delete('/:userId',
    controller.delete);


/**
 * This function comment is parsed by doctrine
 * @route POST /users/login add ne user
 * @group Users
 * @param {string} email.body.required - user email
 * @param {string} password.body.required - user password
 * @returns {object} 200 - An object or user info
 * @returns {Error}  default - Unexpected error
 */
router.post('/login',
    validate(validation.login),
    controller.login);


module.exports = router;
