const express = require('express');
const validate = require('../middlewares/validate');

const router = new express.Router();
const controller = require('../controllers/article');
const validation = require('../validations/article');

/**
 * This function comment is parsed by doctrine
 * @route GET /articles
 * @group Articles
 * @returns {object} 200 - An array of articles
 * @returns {Error}  default - Unexpected error
 */

router.get('/', controller.list);
/**
 * This function comment is parsed by doctrine
 * @route GET /articles/search
 * @group Articles
 * @param {string} text.query.required - search text
 * @returns {object} 200 - An array of articles
 * @returns {Error}  default - Unexpected error
 */

router.get('/search',
    validate(validation.search),
    controller.search);

/**
 * This function comment is parsed by doctrine
 * @route POST /articles create an article
 * @group Articles
 * @param {number} authorId.body.required - author id
 * @param {string} title.body.required - article title
 * @param {string} body.body.required - article body
 * @returns {object} 200 - An object or article info
 * @returns {Error}  default - Unexpected error
 */

router.post('/',
    validate(validation.create),
    controller.create);

/**
 * This function comment is parsed by doctrine
 * @route GET /articles/{articleId} ger article by id
 * @group Articles
 * @param {number} articleId.params.required - article id
 * @returns {object} 200 - An object or article info
 * @returns {Error}  default - Unexpected error
 */

router.get('/:articleId',
    controller.findById);
/**
 * This function comment is parsed by doctrine
 * @route PUT /articles/{articleId} update ana article
 * @group Articles
 * @param {number} articleId.params.required - article id
 * @param {number} authorId.body - author id
 * @param {string} title.body - article title
 * @param {string} body.body - article body
 * @returns {object} 200 - An object or article info
 * @returns {Error}  default - Unexpected error
 */


router.put('/:articleId',
    controller.update);

/**
 * This function comment is parsed by doctrine
 * @route DELETE /articles/{articleId} delete ana article
 * @group Articles
 * @param {number} articleId.params.required - article id
 * @returns {string} 200 - {msg:""}
 * @returns {Error}  default - Unexpected error
 */

router.delete('/:articleId',
    controller.delete);
/**
 * This function comment is parsed by doctrine
 * @route POST /articles/{articleId}/comments add comment to ana article
 * @group Articles
 * @param {number} articleId.body.required - article id
 * @param {string} text.body.required - comment text
 * @param {number} userId.body.required - userId
 * @returns {object} 200 - An object or comment info
 * @returns {Error}  default - Unexpected error
 */

router.post('/:articleId/comments',
    validate(validation.comment),
    controller.comment);
/**
 * This function comment is parsed by doctrine
 * @route GET /articles/{articleId}/comments  get article comments
 * @group Articles
 * @param {number} articleId.body.required - article id
 * @returns {object} 200 - An array of comments
 * @returns {Error}  default - Unexpected error
 */

router.get('/:articleId/thumb',
    controller.articleComments);
/**
 * This function comment is parsed by doctrine
 * @route POST /articles/{articleId}/thumb Thumb up article
 * @group Articles
 * @param {number} articleId.body.required - article id
 * @param {number} userId.body.required - userId
 * @returns {object} 200 - An object or comment info
 * @returns {Error}  default - Unexpected error
 */
router.post('/:articleId/thumb',
    validate(validation.thumb),
    controller.thumb);

module.exports = router;
