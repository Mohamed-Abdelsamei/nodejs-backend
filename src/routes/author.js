const express = require('express');
const validate = require('../middlewares/validate');

const router = new express.Router();
const controller = require('../controllers/author');
const validation = require('../validations/author');

/**
 * This function comment is parsed by doctrine
 * @route GET /authors get all authors
 * @group Authors
 * @returns {object} 200 - An array of authors
 * @returns {Error}  default - Unexpected error
 */
router.get('/',
    controller.list);
/**
 * This function comment is parsed by doctrine
 * @route POST /authors create new author
 * @group Authors
 * @param {string} name.body.required - author name
 * @param {string} title.body.required - author title
 * @param {string} job.body.required - author job
 * @returns {object} 200 - An object of author info
 * @returns {Error}  default - Unexpected error
 */
router.post('/',
    validate(validation.create),
    controller.create);
/**
 * This function comment is parsed by doctrine
 * @route GET /authors/{authorId} ger author by id
 * @group Authors
 * @param {number} authorId.params.required - author id
 * @returns {object} 200 - An object or author info
 * @returns {Error}  default - Unexpected error
 */
router.get('/:authorId',
    controller.findById);
/**
 * This function comment is parsed by doctrine
 * @route PUT /authors/{authorId} update author info
 * @group Authors
 * @param {number} authorId.params.required - author id
 * @param {string} name.body - author name
 * @param {string} title.body - author title
 * @param {string} job.body - author job
 * @returns {object} 200 - An object or author info
 * @returns {Error}  default - Unexpected error
 */
router.put('/:authorId',
    validate(validation.update),
    controller.update);
/**
 * This function comment is parsed by doctrine
 * @route DELETE /authors/{authorId} delete author by id
 * @group Authors
 * @param {number} authorId.params.required - author id
 * @returns {object} 200 - An object or author info
 * @returns {Error}  default - Unexpected error
 */
router.delete('/:authorId',
    controller.delete);

module.exports = router;
