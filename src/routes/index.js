const express = require('express');
const UserRouter = require('./user');
const AuthorRouter = require('./author');
const ArticleRouter = require('./article');

const router = new express.Router();

router.use('/articles', ArticleRouter);
router.use('/users', UserRouter);
router.use('/authors', AuthorRouter);

module.exports = router;
