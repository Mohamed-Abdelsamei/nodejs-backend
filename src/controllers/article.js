/* eslint-disable max-len */
const Article = require('../models/article');
const Comment = require('../models/comment');
const ThumbedArticles = require('../models/thumb');
const HttpError = require('../utils/httpError');

module.exports.list = async (req, res, next) => {
  try {
    const articles = await Article.find();
    return res.json({articles});
  } catch (err) {
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  try {
    const article = await Article.create({...req.body});
    return res.json(article);
  } catch (err) {
    next(err);
  }
};

module.exports.update = async (req, res, next) => {
  try {
    const {articleId} = req.params;
    const article = await Article.updateById( articleId, req.body);
    if (!article) throw new HttpError(404, 'Article not found');
    return res.json({msg: 'article updated'});
  } catch (err) {
    next(err);
  }
};

module.exports.delete = async (req, res, next) => {
  try {
    const {articleId} = req.params;
    const article = await Article.remove( articleId);
    if (!article) throw new HttpError(404, 'Article not found');
    return res.json({msg: 'article removed'});
  } catch (err) {
    next(err);
  }
};

module.exports.findById = async (req, res, next) => {
  try {
    const {articleId} = req.params;
    const article = await Article.findById( articleId);
    if (!article) throw new HttpError(404, 'Article not found');
    return res.json({article});
  } catch (err) {
    next(err);
  }
};

module.exports.search = async (req, res, next) => {
  try {
    const {text}=req.query;
    const articles = await Article.search(text);
    return res.json({articles});
  } catch (err) {
    next(err);
  }
};

module.exports.comment = async (req, res, next) => {
  try {
    const {articleId}=req.params;
    const comment = await Comment.create({...req.body, articleId});
    return res.json({comment});
  } catch (err) {
    next(err);
  }
};

module.exports.articleComments = async (req, res, next) => {
  try {
    const {articleId} = req.params;
    const comments = await Comment.findByArticleId( articleId);
    if (!comments) throw new HttpError(404, 'Article not found');
    return res.json({comments});
  } catch (err) {
    next(err);
  }
};


module.exports.thumb = async (req, res, next) => {
  try {
    const {articleId}=req.params;
    const {userId}=req.body;
    if (!userId ) {
      throw new HttpError(400, 'userId are required');
    }
    const exists = await ThumbedArticles.findByUserIdAndArticleId(userId, articleId);
    if (exists) throw new HttpError(400, 'You can thumb once');
    await ThumbedArticles.create({userId, articleId});
    return res.json({msg: 'Success'});
  } catch (err) {
    next(err);
  }
};
