const Author = require('../models/author');
const HttpError = require('../utils/httpError');

module.exports.list = async (req, res, next) => {
  try {
    const authors = await Author.find();
    return res.json({authors});
  } catch (err) {
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  try {
    const author = await Author.create({...req.body});
    return res.json(author);
  } catch (err) {
    next(err);
  }
};

module.exports.update = async (req, res, next) => {
  try {
    const {authorId} = req.params;
    const author = await Author.updateById(authorId, req.body);
    if (!author) throw new HttpError(404, 'Author not found');
    return res.json({msg: 'author updated'});
  } catch (err) {
    next(err);
  }
};

module.exports.delete = async (req, res, next) => {
  try {
    const {authorId} = req.params;
    const author = await Author.remove(authorId);
    if (!author) throw new HttpError(404, 'Author not found');
    return res.json({msg: 'author removed'});
  } catch (err) {
    next(err);
  }
};

module.exports.findById = async (req, res, next) => {
  try {
    const {authorId} = req.params;
    const author = await Author.findById(authorId);
    if (!author) throw new HttpError(404, 'Author not found');
    return res.json({author});
  } catch (err) {
    next(err);
  }
};
