/* eslint-disable no-unused-vars */
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config');
const HttpError = require('../utils/httpError');
const bcrypt = require('bcryptjs');

module.exports.list = async (req, res, next) => {
  try {
    let users = await User.find();
    users=users.map(({password, ...user})=>user);
    return res.json({users});
  } catch (err) {
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  try {
    if (!req.body.email || !req.body.password) {
      throw new HttpError(400, 'email,password are required');
    }
    const password=await bcrypt.hash(req.body.password, 8);
    const user =await User.create({...req.body, password});

    return res.json(user);
  } catch (err) {
    next(err);
  }
};

module.exports.update = async (req, res, next) => {
  try {
    const {userId} = req.params;
    const user = await User.updateById(userId, req.body);
    if (!user) throw new HttpError(404, 'User not found');
    return res.json({msg: 'user updated'});
  } catch (err) {
    next(err);
  }
};

module.exports.delete = async (req, res, next) => {
  try {
    const {userId} = req.params;
    const user = await User.remove(userId);
    if (!user) throw new HttpError(404, 'User not found');
    return res.json({msg: 'user removed'});
  } catch (err) {
    next(err);
  }
};

module.exports.findById = async (req, res, next) => {
  try {
    const {userId} = req.params;
    const {password, ...user} = await User.findById(userId);
    if (!user) throw new HttpError(404, 'User not found');
    return res.json({user});
  } catch (err) {
    next(err);
  }
};


module.exports.login = async (req, res, next) => {
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email});
    if (!user) throw new HttpError(401, 'Username or password incorrect');
    const valid = await bcrypt.compare(password, user.password);
    if (!valid) throw new HttpError(401, 'Username or password incorrect');
    // Generate an access token
    const token = jwt.sign({id: user.id}, config.accessTokenSecret);

    return res.json({token});
  } catch (err) {
    next(err);
  }
};

module.exports.me = async (req, res, next) => {
  try {
    return res.json({user: req.user});
  } catch (err) {
    next(err);
  }
};
