
/**
 *
 *
 * @class HttpError
 * @extends {Error}
 */
class HttpError extends Error {
  /**
   * Creates an instance of HttpError.
   * @param {*} status
   * @param {*} message
   * @param {*} data
   * @memberof HttpError
   */
  constructor(status, message, data) {
    super(message);
    this.status = status;
    this.message = message;
    this.data = data;
  }
}

module.exports = HttpError;
