
module.exports = (resourceSchema) => async (req, res, next) => {
  const {params, query, body} = req;
  try {
    // throws an error if not valid
    await resourceSchema.validate({params, query, body});
    next();
  } catch (e) {
    console.error(e);
    return res.status(400).json({msg: e.errors.join(', ')});
  }
};
