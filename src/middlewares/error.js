const yup = require('yup');

const error = (error, req, res, next) => {
  // console.log(error);
  if (error instanceof yup.ValidationError) {
    return res.status(400).json({msg: error.message});
    // status code is 400 by default
  }
  return res.status(error.status || 500)
      .json({msg: error.message});
};

const notFound =(req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
};

module.exports={error, notFound};
