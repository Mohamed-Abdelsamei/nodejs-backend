const yup = require('yup');

const validations = {};

validations.create= yup.object().shape({
  body: yup.object().shape({
    name: yup.string().required('name is required'),
    title: yup.string().max(255).required('Title is required'),
    job: yup.string().max(255).required('job is required'),
  }),
});

validations.update= yup.object().shape({
  body: yup.object().shape({
    name: yup.string(),
    title: yup.string().max(255),
    job: yup.string().max(255),
  }),
});


module.exports = validations;
