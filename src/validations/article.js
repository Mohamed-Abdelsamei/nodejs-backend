const yup = require('yup');

const validations = {};

validations.create=yup.object().shape({
  body: yup.object().shape({
    authorId: yup.string().required('authorId is required'),
    title: yup.string().max(255).required('Title is required'),
    body: yup.string().max(255).required('Body is required'),
  }),
});

validations.search= yup.object().shape({
  query: yup.object().shape({
    text: yup.string().required('Please provide search text'),
  }),
});

validations.update= yup.object().shape({
  body: yup.object().shape({
    title: yup.string().max(255),
    body: yup.string().max(255),
  }),
});

validations.comment= yup.object().shape({
  body: yup.object().shape({
    userId: yup.string().required('userId is required'),
    text: yup.string().required('text is required'),
  }),
});
validations.thumb= yup.object().shape({
  body: yup.object().shape({
    userId: yup.string().required('userId is required'),
  }),
});

module.exports = validations;
