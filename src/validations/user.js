const yup = require('yup');

const validations = {};

validations.create= yup.object().shape({
  body: yup.object().shape({
    email: yup.string()
        .email('Please enter a valid email')
        .required('email is required'),
    password: yup.string().min(6, 'password minimum length 6')
        .max(50)
        .required('password is required'),
    firstName: yup.string().max(50).required('firstName is required'),
    lastName: yup.string().max(50).required('lastName is required'),
  }),
});

validations.update= yup.object().shape({
  body: yup.object().shape({
    email: yup.string().email(),
    firstName: yup.string().max(50),
    lastName: yup.string().max(50),
  }),
});


validations.login= yup.object().shape({
  body: yup.object().shape({
    email: yup.string()
        .email('Please enter a valid email')
        .required('email is required'),
    password: yup.string().min(6, 'password minimum length 6')
        .max(50)
        .required('password is required'),
  }),
});


module.exports = validations;
