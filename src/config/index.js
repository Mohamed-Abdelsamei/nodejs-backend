const config = {
  port: process.env.PORT|| 1337,
  db: {
    host: process.env.DB_HOST||'localhost',
    user: process.env.DB_USER||'messam',
    password: process.env.DB_PASSWORD||'secret',
    db: process.env.DB_NAME||'testdb',
    waitForConnections: true,
    namedPlaceholders: true,
    connectionLimit: 2,
    queueLimit: 0,
    debug: true,
  },
  accessTokenSecret: 'asssssddd6575576',
};

module.exports = config;


