/* eslint-disable max-len */
const app = require('../app');
const Author = require('../src/models/author');
const supertest = require('supertest');

let author;
describe('authors', () => {
  beforeEach(async () => {
    author = await Author.create({
      name: 'Mohamed Essam',
      job: 'Software Engineer',
      title: 'Engineer',
    });
    return author;
  });

  afterEach(async (done) => {
    await Author.removeAll();
    done();
  });
  it('GET /api/authors it should respond with array of authors', async (done) => {
    const response = await supertest(app).get('/api/authors');
    // console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(Array.isArray(response.body.authors)).toBeTruthy();
    expect(response.body.authors).toHaveLength(1);

    // Check data
    expect(response.body.authors[0].email).toBe(author.email);
    expect(response.body.authors[0].firstName).toBe(author.firstName);
    expect(response.body.authors[0].lastName).toBe(author.lastName);
    done();
  });
  it('GET /api/authors/:id it should get author by id', async (done) => {
    const response = await supertest(app).get(`/api/authors/${author.id}`);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body.author).toBeTruthy();

    // Check data
    expect(response.body.author.id).toBe(author.id);
    expect(response.body.author.email).toBe(author.email);
    expect(response.body.author.firstName).toBe(author.firstName);
    expect(response.body.author.lastName).toBe(author.lastName);
    done();
  });

  it('PUT /api/authors/:id it should update author info', async (done) => {
    const response = await supertest(app)
        .put(`/api/authors/${author.id}`)
        .send({name: 'ali'});

    const updatedAuthor = await Author.findById(author.id);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    expect(updatedAuthor).toBeTruthy();

    expect(updatedAuthor.name).toBe('ali');

    done();
  });

  it('DELETE /api/authors/:id it should delete author', async (done) => {
    const response = await supertest(app).delete(`/api/authors/${author.id}`);

    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    const deletedAuthor = await Author.findById(author.id);

    expect(deletedAuthor).not.toBeTruthy();

    done();
  });

  it('DELETE /api/authors/:id it should not delete author with wrong id', async (done) => {
    const fakeId = 1;
    const response = await supertest(app).delete(`/api/authors/${fakeId}`);

    expect(response.statusCode).toBe(400);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    const deletedAuthor = await Author.findById(fakeId);

    expect(deletedAuthor).not.toBeTruthy();

    done();
  });
});
