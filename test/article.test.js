/* eslint-disable max-len */
const app = require('../app');
const Article = require('../src/models/article');
const Author = require('../src/models/author');
const supertest = require('supertest');

let article;
let author;
describe('articles', () => {
  beforeEach(async () => {
    author = await Author.create({
      name: 'Mohamed Essam',
      job: 'Software Engineer',
      title: 'Engineer',
    });

    article = await Article.create({
      title: 'new article',
      body: 'article body',
      authorId: author.id,
    });
  });

  afterEach(async (done) => {
    await Article.removeAll();
    await Author.removeAll();
    done();
  });

  it('GET /api/articles it should respond with array of articles', async (done) => {
    const response = await supertest(app).get('/api/articles');
    // console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(Array.isArray(response.body.articles)).toBeTruthy();
    expect(response.body.articles).toHaveLength(1);

    // Check data
    expect(response.body.articles[0].title).toBe(article.title);
    expect(response.body.articles[0].body).toBe(article.body);
    expect(response.body.articles[0].authorId).toBe(article.authorId);
    done();
  });
  it('GET /api/articles/:id it should get article by id', async (done) => {
    const response = await supertest(app).get(`/api/articles/${article.id}`);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body.article).toBeTruthy();

    // Check data
    expect(response.body.article.id).toBe(article.id);
    expect(response.body.article.title).toBe(article.title);
    expect(response.body.article.body).toBe(article.body);
    expect(response.body.article.authorId).toBe(article.authorId);
    done();
  });

  it('PUT /api/articles/:id it should update article info', async (done) => {
    const response = await supertest(app)
        .put(`/api/articles/${article.id}`)
        .send({title: 'new article 1'});

    const updatedArticle = await Article.findById(article.id);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    expect(updatedArticle).toBeTruthy();

    expect(updatedArticle.title).toBe('new article 1');

    done();
  });

  it('DELETE /api/articles/:id it should delete article', async (done) => {
    const response = await supertest(app).delete(`/api/articles/${article.id}`);

    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    const deletedArticle = await Article.findById(article.id);

    expect(deletedArticle).not.toBeTruthy();

    done();
  });

  it('DELETE /api/articles/:id it should not delete article with wrong id', async (done) => {
    const fakeId = 1;
    const response = await supertest(app).delete(`/api/articles/${fakeId}`);

    expect(response.statusCode).toBe(400);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    const deletedArticle = await Article.findById(fakeId);

    expect(deletedArticle).not.toBeTruthy();

    done();
  });
});
