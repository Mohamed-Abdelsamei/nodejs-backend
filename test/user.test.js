const app = require('../app');
const User = require('../src/models/user');
const supertest = require('supertest');

let user;
describe('users', () => {
  beforeEach(async () => {
    user = await User.create({
      email: 'messam@codiles.com',
      firstName: 'hessam',
      lastName: 'hessam',
      password: '$2a$08$9x2g8yTQU2s4ma3xnUPmFODpJc4HMaX1WIuRGhMqDIos8aXRMBNbC',
    });
    return user;
  });

  afterEach(async () => {
    await User.removeAll();
  });

  it('GET /api/users', async (done) => {
    const response = await supertest(app).get('/api/users');
    // console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(Array.isArray(response.body.users)).toBeTruthy();
    expect(response.body.users).toHaveLength(1);

    // Check data
    expect(response.body.users[0].email).toBe(user.email);
    expect(response.body.users[0].firstName).toBe(user.firstName);
    expect(response.body.users[0].lastName).toBe(user.lastName);
    done();
  });

  it('GET /api/users/:id', async (done) => {
    const response = await supertest(app).get(`/api/users/${user.id}`);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body.user).toBeTruthy();
    // Check data
    expect(response.body.user.id).toBe(user.id);
    expect(response.body.user.email).toBe(user.email);
    expect(response.body.user.firstName).toBe(user.firstName);
    expect(response.body.user.lastName).toBe(user.lastName);
    done();
  });

  it('PUT /api/users/:id', async (done) => {
    const response = await supertest(app)
        .put(`/api/users/${user.id}`)
        .send({firstName: 'ali'});

    const updatedUser = await User.findById(user.id);
    //   console.log(response);
    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    expect(updatedUser).toBeTruthy();

    expect(updatedUser.firstName).toBe('ali');

    done();
  });

  it('DELETE /api/users/:id', async (done) => {
    const response = await supertest(app).delete(`/api/users/${user.id}`);
    const deletedUser = await User.findById(user.id);

    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.msg).toBeTruthy();

    expect(deletedUser).not.toBeTruthy();

    done();
  });

  it('pOST /api/users/login', async (done) => {
    const response = await supertest(app)
        .post(`/api/users/login`)
        .send({email: user.email, password: '123456'});

    expect(response.statusCode).toBe(200);
    // Check type and length
    expect(response.body).toBeTruthy();
    // Check data
    expect(response.body.token).toBeTruthy();

    done();
  });
});
