const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const routes = require('./src/routes');
const {error, notFound} = require('./src/middlewares/error');
const app = express();
const expressSwagger = require('express-swagger-generator')(app);

const options = {
  swaggerDefinition: {
    info: {
      description: 'This is a sample server',
      title: 'Swagger',
      version: '1.0.0',
    },
    host: 'localhost:1337',
    basePath: '/api',
    produces: [
      'application/json',
      'application/xml',
    ],
    schemes: ['http'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        name: 'authorization',
        scheme: 'bearer',
        in: 'header',
        description: 'Bearer ...token',
      },
    },
  },
  basedir: __dirname, // app absolute path
  files: ['./src/routes/**/*.js',
    './src/models/**/*.js'], // Path to the API handle folder
};
expressSwagger(options);

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

app.use('/api', routes);

app.use(notFound);

app.use(error);

module.exports = app;
