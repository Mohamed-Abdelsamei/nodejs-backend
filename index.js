const http = require('http');
const app = require('./app');
const config = require('./src/config');

const server = http.createServer(app);
server.listen(config.port, () => {
  console.log('server started');
});
