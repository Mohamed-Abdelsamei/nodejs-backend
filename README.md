# File Structure


    - src
    ---- controllers
    ---- models
    ---- middlewares
    ---- routes
    ---- utils
    ---- config
    ---- validation


# api documentation 
    http://localhost:1337/api-docs

# ER diagram

![ER diagram](https://gitlab.com/MohamedEssam/nodejs-backend/-/raw/master/er-diagram.png)

# Run
```console
$ docker-compose up
```